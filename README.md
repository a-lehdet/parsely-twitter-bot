# Parsely Twitter Bot

Parsely Twitter Bot is a command line Ruby application that retrieves behavioral data from [Parsely](http://www.parsely.com/). 

The application tweets most visited and shared content from each connected website to the timeline of the [@Alehdet](http://twitter.com/Alehdet) account (and [@iLabDev](http://twitter.com/iLabDev) for development purposes). Furthermore, the application is able to interact with other Twitter users by following users who mention the account, or follow back users who are currently following the account.

The commercial purpose of the Parsely Twitter Bot is to improve A-lehdet social media presence and generate traffic and conversions to A-lehdet websites. Currently the websites represented in the application are: 

* [Maku](http://www.maku.fi/)
* [Lily](http://www.lily.fi/)
* [Tuulilasi](http://www.tuulilasi.fi)
* [Urheilulehti](http://www.urheilulehti.fi).

## Requirements

* [Ruby](http://ruby-lang.org/) with the following gems installed.
    * [eat](https://github.com/seamusabshere/eat)
    * [json](http://flori.github.io/json/)
    * [twitter](http://sferik.github.io/twitter/)
    * [colorize](http://github.com/fazibear/colorize)

## Installation

Clone the repository to your local workstation or server.

```git clone https://<USERNAME>@bitbucket.org/a-lehdet/parsely-twitter-bot.git```

Install required gems via Bundler.

```
cd /<PATH>/parsely-twitter-bot/
bundle install
```

**NOTE:** If you have installed Ruby via [RVM](http://rvm.io/), it is highly recommended to let RVM configure the path settings for crontab.

```rvm cron setup```

## Usage

```ruby parsely-twitter-bot.rb [--endpoint] [--timeframe]```

**Example 1 - Most read today**

```ruby parsely-twitter-bot.rb -r -d```

**Example 2 - Most shared this week**

```ruby parsely-twitter-bot.rb -s -w```

### Endpoints

|Short Option |Long Option  |Endpoint           |
| :-          | :-          | :-                |
|-r           |--read       |Most read posts    |
|-s           |--shared     |Most shared posts  |

### Timeframes

|Short Option |Long Option  |Timeframe          |
| :-          | :-          | :-                |
|-d           |--day        |Day                |
|-w           |--week       |Week               |
|-m           |--month      |Month              |
|-y           |--year       |Year               |

### Additional Options
|Short Option |Long Option  |Function           |
| :-          | :-          | :-                |
|-f           |--follow     |Follow back users  |