# coding utf-8
require "rubygems"
require "eat"
require "json"
require "twitter"
require "optparse"
require "colorize"

class ParselyTwitterBot

  # SETUP PARSELY ACCOUNTS & AUTHENTICATE TWITTER
  def initialize

    get_parsely_accounts
    authenticate_twitter

    # Parsely API settings
    @api = {
      :url       => "http://api.parsely.com/v2/",        # Parsely API base-url
      :endpoint  => {:method => false, :label => false}, # Parsely API endpoint (set by command line option)
      :timeframe => {:value => false, :label => false}   # Parsely API timeframe (set by command line option)
    }

    handle_options

  end

  # LOAD PARSELY ACCOUNTS CONFIGURATION FILE
  def get_parsely_accounts
    @parsely_config = JSON.parse(eat("config/parsely.json"))
  end

  # AUTHENTICATE TWITTER API CONNECTION
  def authenticate_twitter

    @twitter_config = JSON.parse(eat("config/twitter.json"))

    @twitter_client = Twitter::REST::Client.new do |config|
      config.consumer_key        = @twitter_config["consumer_key"]
      config.consumer_secret     = @twitter_config["consumer_secret"]
      config.access_token        = @twitter_config["access_token"]
      config.access_token_secret = @twitter_config["access_token_secret"]
    end

  end

  # HANDLE USER INPUT OPTIONS
  def handle_options

    # Command line option parser
    option_parser = OptionParser.new do |option|

      # Endpoint - Most read
      option.on("-r", "--read") do
        @api[:endpoint][:method] = "analytics/posts"
        @api[:endpoint][:label]  = "luetuin"
      end

      # Endpoint - Most shared
      option.on("-s", "--shared") do
        @api[:endpoint][:method] = "shares/posts"
        @api[:endpoint][:label]  = "jaetuin"
      end

      # Timeframe - Day
      option.on("-d", "--day") do
        @api[:timeframe][:value] = 1
        @api[:timeframe][:label] = "päivän"
      end

      # Timeframe - Week
      option.on("-w", "--week") do
        @api[:timeframe][:value] = 7
        @api[:timeframe][:label] = "viikon"
      end

      # Timeframe - Month
      option.on("-m", "--month") do
        @api[:timeframe][:value] = 30
        @api[:timeframe][:label] = "kuukauden"
      end

      # Timeframe - Year
      option.on("-y", "--year") do
        @api[:timeframe][:value] = 365
        @api[:timeframe][:label] = "vuoden"
      end

      # Follow back users
      option.on("-f", "--follow") do
        follow_back
      end

    end

    error = "Missing or invalid options"

    # Invalid option(s)
    begin
      option_parser.parse!(ARGV)
    rescue OptionParser::ParseError
      handle_error(error, true)
    end

    # Proceed if endpoint & timeframe are set
    @api[:endpoint][:method] && @api[:timeframe][:value] ? handle_accounts : handle_error(error, true)

  end

  # DISPLAY ERROR MESSAGE AND HELP (OPTIONAL)
  def handle_error(message, help)

    puts "\nERROR: #{message}\n".red

    if help
      puts "usage: ruby parsely-twitter-bot.rb [--endpoint] [--timeframe]\n".yellow
      puts "ENDPOINTS".yellow
      puts "-r --read     Most read"
      puts "-s --shared   Most shared\n\n"
      puts "TIMEFRAMES".yellow
      puts "-d --day      Day"
      puts "-w --week     Week"
      puts "-m --month    Month"
      puts "-y --year     Year\n\n"
      puts "ADDITIONAL OPTIONS".yellow
      puts "-f --follow   Follow back users\n\n"
    end

    exit

  end

  # GET POSTS FOR EACH ACCOUNT
  def handle_accounts

    # Apply interval to prevent Twitter countermeasures
    @parsely_config.each do|account|
      sleep(2)
      get_post(account[1])
    end

  end

  # GET MOST READ OR SHARED POST FOR ACCOUNT WITHIN TIMEFRAME
  def get_post(account)

    # API call URL parameters
    params = {
      :apikey => account["key"],
      :secret => account["secret"],
      :limit  => 1,
      :page   => 1,
      :days   => @api[:timeframe][:value],
      :pub_days => @api[:timeframe][:value],
      :sort   => "_hits"
    }

    # Form URL query string
    params = params.map{|x,y| "#{x}=#{y}"}.reduce{|x,y| "#{x}&#{y}"}

    # Execute API call & return JSON data
    response = JSON.parse(eat("#{@api[:url]}#{@api[:endpoint][:method]}?#{params}", :timeout => 10))

    # Display error for invalid request
    if response["success"] == false
      handle_error("Invalid request for #{account["key"]}.", false)
    end

    # Parse tweet components
    @title  = parse_title(response["data"][0]["title"])
    @prefix = "#{account["screen_name"]} #{@api[:timeframe][:label]} #{@api[:endpoint][:label]}: #{@title}"
    @url    = response["data"][0]["url"]
    @tags   = parse_tags(response["data"][0]["tags"])
    
    # Send tweet to Twitter profile timeline
    puts "#{@prefix} #{@url.blue} #{@tags.green}"
    #@twitter_client.update("#{@prefix} #{@url} #{@tags}")

  end

  # REMOVE SUBTITLE AND CUT TO MAXIMUM LENGTH
  def parse_title(title)

    title = title.tr("–", "-").split(" - ").first.rstrip
    title = title.slice(0..80) + "..." if title.length > 80
    title

  end

  # PARSE PARSELY TAGS TO HASHTAG STRING
  def parse_tags(tags)

    @counter   = 0
    @tags_temp = tags.shuffle
    hashtags   = parse_hashtags(@prefix.length + 1, 23)

    # Prevent mysterious empty string-array thingys
    hashtags == "#[]" ? "" : hashtags

  end

  # LEAVE OUT HASHTAG(S) IF TWEET IS TOO LONG
  def parse_hashtags(name_length, url_length)

    hashtags = ""

    # Parse tag array to hashtags string
    @tags_temp.each do |tag|
      hashtags << "##{tag.to_s.downcase.tr("-", "").tr(" ", "").tr("&", "ja").tr("_", "")} "
    end

    hashtags.rstrip!

    # Delete last tag and execute recursion if tweet is too long
    if (name_length + url_length + hashtags.length).to_i > 140
      @tags_temp.pop
      @counter += 1
      parse_hashtags(name_length, url_length) unless @counter > 10 # Prevent stack lever errors
    else
      hashtags
    end

  end

  # FOLLOW BACK USERS WHO INTERACT WITH CURRENT USER
  def follow_back

    # Fetch list of current followers
    @twitter_followers = @twitter_client.followers(@twitter_config["screen_name"])

    # Follow back people who follow current user
    @twitter_followers.each do |follower|
      follow_user(follower)
    end

    # Follow users who mention current user
    @twitter_client.mentions_timeline.each do |mention|
      follow_user(mention.user)
    end

  end

  # FOLLOW USER ON TWITTER
  def follow_user(user)

    followers = ""

    # From array from current followers
    @twitter_followers.each do |follower|
      followers << follower.screen_name
    end

    # Follow user only if not already following
    if followers.include?(user.screen_name)
      puts "Already following user #{user.screen_name.blue}"
    else
      begin
        @twitter_client.follow(user.screen_name)
        puts "Following user #{user.screen_name.blue}"
      rescue
        handle_error("Unable to follow user #{user.screen_name}", false)
      end
    end

  end

end

ParselyTwitterBot.new